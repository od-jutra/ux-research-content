console.log('connected!')
// window scroll
function scrollWin(){
    window.scrollBy(0, 700);
}

let pageOne = document.querySelector('.one')
let boxOne = pageOne.querySelectorAll('div.wrapper > div');
let articlesOne = ['a1', 'a2', 'a3', 'a4', 'a5', 'a6'];
let tag = ['#NEWS', '#LIFESTYLE', '#ROZRYWKA', '#TECHNOLOGIA', '#KULTURA', '#MOJE MIASTO'];

//assigning randomly articlesOne


function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
  
    return array;
  };

shuffle(articlesOne);
console.log(articlesOne);
//matching box with mixed articles
function boxMatch(box, article){
    for(i = 0; i<6; i++){
        box[i].classList.add(article[i])
    }
};
boxMatch(boxOne, articlesOne);
// showing the article code
for (var i = 0; i < 6; i++){
    boxOne[i].addEventListener('click', addArtCode);
    function addArtCode(){
        if(this.classList.contains('chosen')){
            this.classList.remove('chosen')
            }
        else{
            this.classList.add('chosen');
            var classes = this.className.split(' ');
            this.innerHTML += '<p class="code">' + classes[1]+ '</p>';
        }
    }
};
// content for the first page
let contentOne= [
    'Kraje, do których pojedziemy. Pojawiły się nowe informacje',
    'Jak zwiedzić środkową Amerykę za 17 dolarów dziennie',
    'Czym jest ostry cień mgły? Zebraliśmy reakcje na #Hot16Challenge Andrzeja Dudy',
    'Jaką hulajnogę elektryczną wybrać? Top 10 modeli',
    'Nowa praca Banksy\'ego w szpitalu. Pokazuje, kto jest prawdziwym superbohaterem',
    'Pięć ogródków restauracyjnych w twoim mieście, które tętnią życiem już dzisiaj'
]
//assigning  content to articlesOneOne page one
    for( var i=1; i<7; i++){
        let place = document.querySelector('.a'+ i);
        console.log(place);
        place.innerHTML += '<p class = "tag">' + tag[i-1] + '</p><p class = "header">' + contentOne[i-1] + '</p><br> <p class = "read"> więcej ></p>';
        place.style.padding = '15px';
    };
// faces scale - voting
let face = document.querySelectorAll('div.navbar > img');
console.log(face);
for (var i = 0; i< 25; i++){
    face[i].addEventListener('click', select);
    function select(){
        if(this.classList.contains('selected')){
            this.classList.remove('selected');
            this.style.border = '';
            }
        else{
        this.classList.add('selected');
        this.style.border = 'dotted';
        this.style.borderColor = 'gray'
        }
    }
}

//---------------------------------------------------------page zero - test your content preferences----------------------------------------
let pageZero = document.querySelector('.zero')
let boxZero = pageZero.querySelectorAll('div.wrapper > div');
let articlesZero = ['z1', 'z2', 'z3', 'z4', 'z5', 'z6', 'z7', 'z8', 'z9', 'z10', 'z11', 'z12'];

shuffle(articlesZero);
//assigning randomly articles
function smallBoxMatch(box, article){
    for(i = 0; i<12; i++){
        box[i].classList.add(article[i])
    }
};
smallBoxMatch(boxZero, articlesZero);

console.log(articlesZero);

for (var i = 0; i < 12; i++){
    boxZero[i].addEventListener('click', addArtCode);
    function addArtCode() {
        if(this.classList.contains('chosen')){
            this.classList.remove('chosen')
            }
        else{
        this.classList.add('chosen');
        var classes = this.className.split(' ');
        this.innerHTML += '<p class="code">' + classes[1]+ '</p>';
        }
    }
};

let contentZero= [
    '#NEWS', '#LIFESTYLE', '#ROZRYWKA', '#TECHNOLOGIA', '#KULTURA', '#MOJE MIASTO', '#SPORT', '#Z ŻYCIA GWIAZD', '#MUZYKA', '#TRENDY', '#POLITYKA', '#NAUKA'
]
//assigning  content to articles page one
for( var i=1; i<13; i++){
    let place = document.querySelector('.z'+ i);
    console.log(place);
    place.innerHTML += '<p class = "header">' + contentZero[i-1] + '</p>';
    place.style.padding = '15px';
};
// --------------------------------------------------page two-----------------------------------------------------------------------------------

let pageTwo = document.querySelector('.two')
let boxTwo = pageTwo.querySelectorAll('div.wrapper > div');
let articlesTwo = ['b1', 'b2', 'b3', 'b4', 'b5', 'b6'];


shuffle(articlesTwo);
//assigning randomly articles
boxMatch(boxTwo, articlesTwo);

console.log(articlesTwo);

for (var i = 0; i < 6; i++){
    boxTwo[i].addEventListener('click', addArtCode);
    function addArtCode() {
        if(this.classList.contains('chosen')){
            this.classList.remove('chosen')
            }
        else{
        this.classList.add('chosen');
        var classes = this.className.split(' ');
        this.innerHTML += '<p class="code">' + classes[1]+ '</p>';
        }
    }
}

let contentTwo= [
    '16-latek za kierownicą. 3 osoby nie żyją.',
    'Znienawidzony typ samochodów. Sięga po niego coraz więcej klientów.',
    'Gdańskie zoo w #hot16challenge2',
    'Słuchawki do treningu. Jakie wybrać do danej aktywności?',
    'Hip hop online. Oglądaj koncerty z całego świata',
    'Plaża w samym środku miasta. Mieszkańcy oburzeni'
]
//assigning  content to articles page one
for( var i=1; i<7; i++){
    let place = document.querySelector('.b'+ i);
    console.log(place);
    place.innerHTML += '<p class = "tag">' + tag[i-1] + '</p><p class = "header">' + contentTwo[i-1] + '</p><br> <p class = "read"> więcej ></p>';
    place.style.padding = '15px';
}
// --------------------------------------------------page three-----------------------------------------------------------------------------------

let pageThree = document.querySelector('.three')
let boxThree = pageThree.querySelectorAll('div.wrapper > div');
let articlesThree = ['c1', 'c2', 'c3', 'c4', 'c5', 'c6'];

shuffle(articlesThree);
//assigning randomly articles
boxMatch(boxThree, articlesThree);

console.log(articlesThree);

for (var i = 0; i < 6; i++){
    boxThree[i].addEventListener('click', addArtCode);
    function addArtCode() {
        if(this.classList.contains('chosen')){
            this.classList.remove('chosen')
            }
        else{
        this.classList.add('chosen');
        var classes = this.className.split(' ');
        this.innerHTML += '<p class="code">' + classes[1]+ '</p>';
        }
    }
}

let contentThree= [
    'Młodzi widzą ignorancję MEN. Piszą list do ministra w sprawie edukacji klimatycznej',
    'Skąd mam wiedzieć, który zawód jest dla mnie odpowiedni?',
    'Piękno mikro świata. Zobacz zdjęcia z mikroskopu elektronowego',
    'Creative Sound Blaster-gdy zintegrowana karta to za mało',
    'Ryuichi Sakamoto w izolacji. Zobacz niezwykły koncert japońskiego kompozytora',
    'Wolontariat w czasie pandemii. Zobacz jak pomagamy sąsiadom'
]
//assigning  content to articlesOneOne page one
for( var i=1; i<7; i++){
    let place = document.querySelector('.c'+ i);
    console.log(place);
    place.innerHTML += '<p class = "tag">' + tag[i-1] + '</p><p class= "header">' + contentThree[i-1] + '</p><br><p class = "read"> więcej ></p>';
    place.style.padding = '15px';
}
// --------------------------------------------------page four-----------------------------------------------------------------------------------

let pageFour = document.querySelector('.four')
let boxFour = pageFour.querySelectorAll('div.wrapper > div');
let articlesFour = ['d1', 'd2', 'd3', 'd4', 'd5', 'd6'];

shuffle(articlesFour);
//assigning randomly articlesOne
boxMatch(boxFour, articlesFour);

console.log(articlesFour);

for (var i = 0; i < 6; i++){
    boxFour[i].addEventListener('click', addArtCode);
    function addArtCode() {
        if(this.classList.contains('chosen')){
            this.classList.remove('chosen')
            }
        else{
        this.classList.add('chosen');
        var classes = this.className.split(' ');
        this.innerHTML += '<p class="code">' + classes[1]+ '</p>';
        }
    }
}

let contentFour= [
    'Poszedłem tam i zacząłem płakać. To nieludzkie miejsce. I nikt nie bierze za to odpowiedzialności',
    'Jak 24-latek w niespełna dwa lata zbudował w USA firmę wycenianą na ponad 11 mln dolarów',
    '"Polski rynek sztuki ciągle jest zacofany i prowincjonalny ..."',
    'Atak hakerów na super komputery',
    'Organizatorzy festiwali biją na alarm. Jest list otwarty do premiera Morawieckiego',
    'Trzy typy (nie)noszących maski w komunikacji '
]
//assigning  content to articlesOneOne page one
for( var i=1; i<7; i++){
    let place = document.querySelector('.d'+ i);
    console.log(place);
    place.innerHTML += '<p class = "tag">' + tag[i-1] + '</p><p class= "header">' + contentFour[i-1] + '</p><br><p class = "read"> więcej ></p>';
    place.style.padding = '15px';
}
// --------------------------------------------------page five-----------------------------------------------------------------------------------

let pageFive = document.querySelector('.five')
let boxFive = pageFive.querySelectorAll('div.wrapper > div');
let articlesFive = ['e1', 'e2', 'e3', 'e4', 'e5', 'e6'];

shuffle(articlesFive);
//assigning randomly articlesOne
boxMatch(boxFive, articlesFive);

console.log(articlesFive);

for (var i = 0; i < 6; i++){
    boxFive[i].addEventListener('click', addArtCode);
    function addArtCode() {
        if(this.classList.contains('chosen')){
            this.classList.remove('chosen')
            }
        else{
        this.classList.add('chosen');
        var classes = this.className.split(' ');
        this.innerHTML += '<p class="code">' + classes[1]+ '</p>';
        }
    }
}

let contentFive= [
    'Koronawirus atakuje dzieci. Straszliwe objawy. Lekarze ostrzegają',
    'Ta dieta na mnie zadziałała. Może zadziałać również na ciebie!',
    'Pandemię spowodowały sieci 5G? Wielu w to wierzy',
    'Quiz – Polegniesz już na pierwszym pytaniu!',
    'Spektakle teatralne 18+',
    'Jak miasto nie kupi, to policja nie ma'
]
//assigning  content to articlesOneOne page one
for( var i=1; i<7; i++){
    let place = document.querySelector('.e'+ i);
    console.log(place);
    place.innerHTML += '<p class = "tag">' + tag[i-1] + '</p><p class= "header">' + contentFive[i-1] + '</p><br><p class = "read"> więcej ></p>';
    place.style.padding = '15px';
}
//---------------------------------------------------page six--------------------------------------------

// let pageSix = document.querySelector('.six')
// let boxSix = pageSix.querySelectorAll('div.wrapper > div');
// let articlesSix = ['f1', 'f2', 'f3', 'f4', 'f5', 'f6'];

// shuffle(articlesSix);
// //assigning randomly articlesOne
// boxMatch(boxSix, articlesSix);

// console.log(articlesSix);

// for (var i = 0; i < 6; i++){
//     boxSix[i].addEventListener('click', addArtCode);
//     function addArtCode() {
//         if(this.classList.contains('chosen')){
//             this.classList.remove('chosen')
//             }
//         else{
//         this.classList.add('chosen');
//         var classes = this.className.split(' ');
//         this.innerHTML += '<p class="code">' + classes[1]+ '</p>';
//         }
//     }
// }

// let contentSix= [
//     'Koronawirus atakuje dzieci. Straszliwe objawy. Lekarze ostrzegają',
//     '/"Ta dieta na mnie zadziałała"/ Może zadziałać również na ciebie!',
//     'Pandemię spowodowały sieci 5G? Wielu w to wierzy',
//     'Quiz – Polegniesz już na pierwszym pytaniu!',
//     'Spektakle teatralne 18+',
//     'Jak miasto nie kupi, to policja nie ma'
// ]
// //assigning  content to articlesOneOne page one
// for( var i=1; i<7; i++){
//     let place = document.querySelector('.f'+ i);
//     console.log(place);
//     place.innerHTML += '<p class = "tag">' + tag[i-1] + '</p><p class= "header">' + contentSix[i-1] + '</p><br><p class = "read"> więcej ></p>';
//     place.style.padding = '15px';
// }
//----------------------------------------------page seven----------------------------------------------------------------------------
let pageSeven = document.querySelector('.seven')
let boxSeven = pageSeven.querySelectorAll('div.wrapper > div');
let articlesSeven = ['g1', 'g2', 'g3', 'g4', 'g5', 'g6'];

shuffle(articlesSeven);
//assigning randomly articlesOne
boxMatch(boxSeven, articlesSeven);

console.log(articlesSeven);

for (var i = 0; i < 6; i++){
    boxSeven[i].addEventListener('click', addArtCode);
    function addArtCode() {
        if(this.classList.contains('chosen')){
            this.classList.remove('chosen')
            }
        else{
        this.classList.add('chosen');
        var classes = this.className.split(' ');
        this.innerHTML += '<p class="code">' + classes[1]+ '</p>';
        }
    }
}

let contentSeven= [
    'Youtuberzy rozwiązują testy na prawo jazdy',
    'Tylko 1/3 kierowców zdałaby najnowszy test na prawo jazdy. A Ty?',
    'Znowu zamieszanie wokół popularnego egzaminu. Jak uniknąć wpadki?',
    'Egzamin na prawo jazdy: pytania łatwe, trudne i... dziwne. Zdasz?',
    'Te testy na prawo jazdy zawstydziły nawet instruktorów...',
    'Egzaminator podpowiada, jak zdobyć prawko.'
]
//assigning  content to articlesOneOne page one
for( var i=1; i<7; i++){
    let place = document.querySelector('.g'+ i);
    console.log(place);
    place.innerHTML += '<br><p class= "header">' + contentSeven[i-1] + '</p><br><p class = "read"> więcej ></p>';
    place.style.padding = '15px';
}
//---------------------------------------------------------------------page eight------------------------------------------

let pageEight = document.querySelector('.eight')
let boxEight = pageEight.querySelectorAll('div.wrapper > div');
let articlesEight = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'];

shuffle(articlesEight);

//assigning randomly articlesOne
boxMatch(boxEight, articlesEight);

console.log(articlesEight);

for (var i = 0; i < 6; i++){
    boxEight[i].addEventListener('click', addArtCode);
    function addArtCode() {
        if(this.classList.contains('chosen')){
            this.classList.remove('chosen')
            }
        else{
        this.classList.add('chosen');
        var classes = this.className.split(' ');
        this.innerHTML += '<p class="code">' + classes[1]+ '</p>';
        }
    }
}
let contentEight= [
    '<img src="pic/artykul.png" alt="typy kontentu">',
    '<img src="pic/grafika.png" alt="typy kontentu">',
    '<img src="pic/kafelek.png" alt="typy kontentu">',
    '<img src="pic/live.png" alt="typy kontentu">',
    '<img src="pic/podcast.png" alt="typy kontentu">',
    '<img src="pic/video.png" alt="typy kontentu">'
]
//assigning  content to articles
for( var i=1; i<7; i++){
    let place = document.querySelector('.h'+ i);
    place.innerHTML += contentEight[i-1];
    place.style.padding = '15px';
}
//-------------------------------------------page nine-------------------------------UI + adjectives to describe-----------------
let pageNine =  document.querySelector('.nine')
let checkboxes = pageNine.querySelectorAll('div.adjective');
let placeholder = ['n1', 'n2', 'n3', 'n4', 'n5', 'n6', 'n7', 'n8', 'n9', 'n10','n11', 'n12', 'n13', 'n14', 'n15', 'n16', 'n17', 'n18', 'n19', 'n20', 'n21', 'n22', 'n23', 'n24', 'n25', 'n26', 'n27', 'n28', 'n29', 'n30', 'n31', 'n32', 'n33', 'n34', 'n35', 'n36']

let adjectives = [
'lojalny','godny zaufania',
'sumienny','pewny siebie',
'dziki','ambitny',
'twórczy','zarządca',
'idealista' ,'ciepły',
'zuchwały','psotny',
'doradca','hojny',
'optymistyczny','przyjacielski',
'honorowy','z poczuciem humoru',
'inteligentny','opiekuńczy',
'przedsiębiorczy','głodny przygód',
'pomysłowy','oddany',
'zorganizowany','charyzmatyczny',
'romantyczny','niezależny',
'pionier','wzór do naśladowania',
'intymny','pragnący zmian',
'empatyczny','dobrze poinformowany',
'odważny','uduchowiony'
];

shuffle(placeholder);

//assigning randomly adjectives
function checkboxMatch(){
    for(i = 0; i<36; i++){
        checkboxes[i].classList.add(placeholder[i])
    }
};
checkboxMatch();
//assigning  content to articles
for( var i=1; i<36; i++){
    let place = document.querySelector('.n'+ i);
    console.log(place);
    place.innerHTML += `<input type="checkbox" id="${adjectives[i-1]}">
    <label for="${adjectives[i-1]}">${adjectives[i-1]}</label>`;
}